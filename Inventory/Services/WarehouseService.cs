﻿using Inventory.Models;
using Inventory.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inventory.Services
{
    public class WarehouseService
    {
        private readonly DataContext db;

        public WarehouseService(DataContext db)
        {
            this.db = db;
        }

        public Warehouse Get(Guid id)
        {
            return db.Warehouse.Find(id);
        }

        public List<Warehouse> GetList()
        {
            return db.Warehouse.OrderBy(x => x.Code).ToList();
        }

        public List<Warehouse> GetList(string keyword)
        {
            var query = db.Warehouse.AsQueryable();

            if (!string.IsNullOrWhiteSpace(keyword))
                query = query.Where(x => x.Code.Contains(keyword) || x.Name.Contains(keyword));

            return query.OrderBy(x => x.Code).ToList();
        }

        private void Validate(Warehouse warehouseResource)
        {
            if (string.IsNullOrWhiteSpace(warehouseResource.Code))
                throw new Exception("Code cannot be empty.");

            if (string.IsNullOrWhiteSpace(warehouseResource.Name))
                throw new Exception("Name cannot be empty.");

            var alreadyExist = db.Warehouse.Any(x => x.Code == warehouseResource.Code && x.Id != warehouseResource.Id);
            if (alreadyExist) throw new Exception("Code is already exist.");
        }

        public Guid Create(Warehouse warehouseResource)
        {
            Validate(warehouseResource);

            var warehouseModel = new Warehouse();
            warehouseModel.Id = Guid.NewGuid();
            warehouseModel.Code = warehouseResource.Code;
            warehouseModel.Name = warehouseResource.Name;
            db.Warehouse.Add(warehouseModel);
            db.SaveChanges();

            return warehouseModel.Id;
        }

        public Guid Update(Warehouse warehouseResource)
        {
            Validate(warehouseResource);

            var warehouseModel = db.Warehouse.Find(warehouseResource.Id);
            warehouseModel.Code = warehouseResource.Code;
            warehouseModel.Name = warehouseResource.Name;
            db.SaveChanges();

            return warehouseModel.Id;
        }

        public void Delete(Guid id)
        {
            var warehouseModel = db.Warehouse.Find(id);
            db.Remove(warehouseModel);
            db.SaveChanges();
        }
    }
}
