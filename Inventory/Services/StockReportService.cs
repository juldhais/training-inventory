﻿using Inventory.Repositories;
using Inventory.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inventory.Services
{
    public class StockReportService
    {
        private readonly DataContext db;

        public StockReportService(DataContext db)
        {
            this.db = db;
        }

        public List<StockReportResource> GetReport(Guid? itemId, Guid? warehouseId, DateTime? startDate, DateTime? endDate)
        {
            if (itemId == null
             && warehouseId == null
             && startDate == null
             && endDate == null) return new List<StockReportResource>();

            if (startDate == null)
                throw new Exception("Start Date cannot be empty.");

            if (endDate == null)
                throw new Exception("End Date cannot be empty.");

            var queryItem = db.Item.AsQueryable();
            var queryWarehouse = db.Warehouse.AsQueryable();

            if (itemId != null)
                queryItem = queryItem.Where(x => x.Id == itemId);
            
            if (warehouseId != null)
                queryWarehouse = queryWarehouse.Where(x => x.Id == warehouseId);
            
            var listItem = queryItem.OrderBy(x => x.Code).ToList();
            var listWarehouse = queryWarehouse.OrderBy(x => x.Code).ToList();

            var result = new List<StockReportResource>();

            foreach (var item in listItem)
            {
                foreach (var warehouse in listWarehouse)
                {
                    var detail = new StockReportResource();
                    detail.ItemCode = item.Code;
                    detail.ItemName = item.Name;
                    detail.WarehouseCode = warehouse.Code;
                    detail.WarehouseName = warehouse.Name;

                    var beginningGR = db.GoodsReceiptDetail
                                        .Where(x => x.GoodsReceipt.DocumentDate.Date < startDate 
                                                 && x.ItemId == item.Id 
                                                 && x.GoodsReceipt.WarehouseId == warehouse.Id)
                                        .Sum(x => x.Quantity);

                    var beginningGI = db.GoodsIssueDetail
                                        .Where(x => x.GoodsIssue.DocumentDate.Date < startDate 
                                                 && x.ItemId == item.Id
                                                 && x.GoodsIssue.WarehouseId == warehouse.Id)
                                        .Sum(x => x.Quantity);

                    detail.Beginning = beginningGR - beginningGI;

                    detail.In = db.GoodsReceiptDetail
                                    .Where(x => x.GoodsReceipt.DocumentDate.Date >= startDate 
                                             && x.GoodsReceipt.DocumentDate.Date <= endDate
                                             && x.ItemId == item.Id
                                             && x.GoodsReceipt.WarehouseId == warehouse.Id)
                                    .Sum(x => x.Quantity);

                    detail.Out = db.GoodsIssueDetail
                                    .Where(x => x.GoodsIssue.DocumentDate.Date >= startDate
                                             && x.GoodsIssue.DocumentDate.Date <= endDate
                                             && x.ItemId == item.Id
                                             && x.GoodsIssue.WarehouseId == warehouse.Id)
                                    .Sum(x => x.Quantity);

                    detail.Ending = detail.Beginning + detail.In - detail.Out;

                    result.Add(detail);
                }
            }

            return result;
        }
    }
}
