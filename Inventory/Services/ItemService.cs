﻿using Inventory.Models;
using Inventory.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inventory.Services
{
    public class ItemService
    {
        private readonly DataContext db;

        public ItemService(DataContext db)
        {
            this.db = db;
        }

        public Item Get(Guid id)
        {
            return db.Item.Find(id);
        }

        public List<Item> GetList()
        {
            return db.Item.OrderBy(x => x.Code).ToList();
        }

        public List<Item> GetList(string keyword)
        {
            var query = db.Item.AsQueryable();

            if (!string.IsNullOrWhiteSpace(keyword))
                query = query.Where(x => x.Code.Contains(keyword) || x.Name.Contains(keyword));

            return query.OrderBy(x => x.Code).ToList();
        }

        private void Validate(Item itemResource)
        {
            if (string.IsNullOrWhiteSpace(itemResource.Code))
                throw new Exception("Code cannot be empty.");

            if (string.IsNullOrWhiteSpace(itemResource.Name))
                throw new Exception("Name cannot be empty.");

            var alreadyExist = db.Item.Any(x => x.Code == itemResource.Code && x.Id != itemResource.Id);
            if (alreadyExist) throw new Exception("Code is already exist.");
        }

        public Guid Create(Item itemResource)
        {
            Validate(itemResource);
            
            var itemModel = new Item();
            itemModel.Id = Guid.NewGuid();
            itemModel.Code = itemResource.Code;
            itemModel.Name = itemResource.Name;
            itemModel.Unit = itemResource.Unit;
            db.Item.Add(itemModel);
            db.SaveChanges();

            return itemModel.Id;
        }

        public Guid Update(Item itemResource)
        {
            Validate(itemResource);

            var itemModel = db.Item.Find(itemResource.Id);
            itemModel.Code = itemResource.Code;
            itemModel.Name = itemResource.Name;
            itemModel.Unit = itemResource.Unit;
            db.SaveChanges();

            return itemModel.Id;
        }

        public void Delete(Guid id)
        {
            var itemModel = db.Item.Find(id);
            db.Remove(itemModel);
            db.SaveChanges();
        }
    }
}
