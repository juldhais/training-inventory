﻿using Inventory.Models;
using Inventory.Repositories;
using Inventory.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inventory.Services
{
    public class GoodsIssueService
    {
        private readonly DataContext db;

        public GoodsIssueService(DataContext db)
        {
            this.db = db;
        }

        public GoodsIssueResource Get(Guid? id)
        {
            var goodsIssue = db.GoodsIssue
                                 .Where(x => x.Id == id)
                                 .Select(x => new GoodsIssueResource
                                 {
                                     Id = x.Id,
                                     DocumentNumber = x.DocumentNumber,
                                     DocumentDate = x.DocumentDate,
                                     WarehouseCode = x.Warehouse.Code,
                                     WarehouseName = x.Warehouse.Name,
                                     WarehouseId = x.WarehouseId,
                                     Remarks = x.Remarks
                                 }).FirstOrDefault();

            goodsIssue.ListDetail = db.GoodsIssueDetail
                                        .Where(x => x.GoodsIssueId == id)
                                        .OrderBy(x => x.Item.Code)
                                        .Select(x => new GoodsIssueDetailResource
                                        {
                                            Id = x.Id,
                                            GoodsIssueId = x.GoodsIssueId,
                                            ItemCode = x.Item.Code,
                                            ItemName = x.Item.Name,
                                            ItemId = x.ItemId,
                                            Unit = x.Item.Unit,
                                            Quantity = x.Quantity
                                        }).ToList();

            return goodsIssue;
        }

        public List<GoodsIssueResource> GetList(Guid? warehouseId, DateTime? startDate, DateTime? endDate)
        {
            var query = db.GoodsIssue.AsQueryable();

            if (warehouseId != null)
                query = query.Where(x => x.WarehouseId == warehouseId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            var list = query.OrderBy(x => x.DocumentNumber)
                            .Select(x => new GoodsIssueResource
                            {
                                Id = x.Id,
                                DocumentNumber = x.DocumentNumber,
                                DocumentDate = x.DocumentDate,
                                WarehouseCode = x.Warehouse.Code,
                                WarehouseName = x.Warehouse.Name,
                                Remarks = x.Remarks,
                                WarehouseId = x.WarehouseId
                            }).ToList();

            return list;
        }

        private string GetNewDocumentNumber()
        {
            var lastNumber = db.GoodsIssue
                               .OrderByDescending(x => x.DocumentNumber)
                               .Select(x => x.DocumentNumber)
                               .FirstOrDefault();

            if (string.IsNullOrWhiteSpace(lastNumber)) return "GR0001";

            var newSequence = Convert.ToInt32(lastNumber.Substring(2, 4)) + 1;

            var newNumber = $"GR{newSequence.ToString("0000")}";

            return newNumber;
        }

        private void Validate(GoodsIssueResource grRes)
        {
            if (grRes.DocumentDate == default)
                throw new Exception("Document Date cannot be empty.");

            if (grRes.WarehouseId == null)
                throw new Exception("Warehouse cannot be empty");

            foreach (var detail in grRes.ListDetail)
            {
                if (detail.ItemId == null)
                    throw new Exception("Item cannot be empty.");

                if (detail.Quantity == 0)
                    throw new Exception("Quantity cannot be empty.");
            }
        }

        public Guid Create(GoodsIssueResource grRes)
        {
            Validate(grRes);

            var gr = new GoodsIssue();
            gr.Id = Guid.NewGuid();
            gr.DocumentNumber = GetNewDocumentNumber();
            gr.DocumentDate = grRes.DocumentDate;
            gr.WarehouseId = grRes.WarehouseId;
            gr.Remarks = grRes.Remarks;
            db.GoodsIssue.Add(gr);

            foreach (var detailRes in grRes.ListDetail)
            {
                var detail = new GoodsIssueDetail();
                detail.Id = Guid.NewGuid();
                detail.GoodsIssueId = gr.Id;
                detail.ItemId = detailRes.ItemId;
                detail.Quantity = detailRes.Quantity;
                db.GoodsIssueDetail.Add(detail);
            }

            db.SaveChanges();

            return gr.Id;
        }

        public Guid Update(GoodsIssueResource grRes)
        {
            Validate(grRes);

            // delete old detail
            var listOldDetailId = db.GoodsIssueDetail
                                    .Where(x => x.GoodsIssueId == grRes.Id)
                                    .Select(x => x.Id)
                                    .ToList();

            foreach (var detailId in listOldDetailId)
            {
                var detail = db.GoodsIssueDetail.Find(detailId);
                db.GoodsIssueDetail.Remove(detail);
            }
            // end delete old detail

            // update
            var gr = db.GoodsIssue.Find(grRes.Id);
            gr.DocumentDate = grRes.DocumentDate;
            gr.WarehouseId = grRes.WarehouseId;
            gr.Remarks = grRes.Remarks;

            foreach (var detailRes in grRes.ListDetail)
            {
                var detail = new GoodsIssueDetail();
                detail.Id = Guid.NewGuid();
                detail.GoodsIssueId = gr.Id;
                detail.ItemId = detailRes.ItemId;
                detail.Quantity = detailRes.Quantity;
                db.GoodsIssueDetail.Add(detail);
            }

            db.SaveChanges();

            return gr.Id;
        }

        public void Delete(Guid? id)
        {
            var listOldDetailId = db.GoodsIssueDetail
                                    .Where(x => x.GoodsIssueId == id)
                                    .Select(x => x.Id)
                                    .ToList();

            foreach (var detailId in listOldDetailId)
            {
                var detail = db.GoodsIssueDetail.Find(detailId);
                db.GoodsIssueDetail.Remove(detail);
            }

            var gr = db.GoodsIssue.Find(id);
            db.GoodsIssue.Remove(gr);

            db.SaveChanges();
        }
    }
}
