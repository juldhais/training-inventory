﻿using Inventory.Models;
using Inventory.Repositories;
using Inventory.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inventory.Services
{
    public class GoodsReceiptService
    {
        private readonly DataContext db;

        public GoodsReceiptService(DataContext db)
        {
            this.db = db;
        }

        public GoodsReceiptResource Get(Guid? id)
        {
            var goodsReceipt = db.GoodsReceipt
                                 .Where(x => x.Id == id)
                                 .Select(x => new GoodsReceiptResource
                                 {
                                     Id = x.Id,
                                     DocumentNumber = x.DocumentNumber,
                                     DocumentDate = x.DocumentDate,
                                     WarehouseCode = x.Warehouse.Code,
                                     WarehouseName = x.Warehouse.Name,
                                     WarehouseId = x.WarehouseId,
                                     Remarks = x.Remarks
                                 }).FirstOrDefault();

            goodsReceipt.ListDetail = db.GoodsReceiptDetail
                                        .Where(x => x.GoodsReceiptId == id)
                                        .OrderBy(x => x.Item.Code)
                                        .Select(x => new GoodsReceiptDetailResource
                                        {
                                            Id = x.Id,
                                            GoodsReceiptId = x.GoodsReceiptId,
                                            ItemCode = x.Item.Code,
                                            ItemName = x.Item.Name,
                                            ItemId = x.ItemId,
                                            Unit = x.Item.Unit,
                                            Quantity = x.Quantity
                                        }).ToList();

            return goodsReceipt;
        }

        public List<GoodsReceiptResource> GetList(Guid? warehouseId, DateTime? startDate, DateTime? endDate)
        {
            var query = db.GoodsReceipt.AsQueryable();

            if (warehouseId != null)
                query = query.Where(x => x.WarehouseId == warehouseId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            var list = query.OrderBy(x => x.DocumentNumber)
                            .Select(x => new GoodsReceiptResource
                            {
                                Id = x.Id,
                                DocumentNumber = x.DocumentNumber,
                                DocumentDate = x.DocumentDate,
                                WarehouseCode = x.Warehouse.Code,
                                WarehouseName = x.Warehouse.Name,
                                Remarks = x.Remarks,
                                WarehouseId = x.WarehouseId
                            }).ToList();

            return list;
        }

        private string GetNewDocumentNumber()
        {
            var lastNumber = db.GoodsReceipt
                               .OrderByDescending(x => x.DocumentNumber)
                               .Select(x => x.DocumentNumber)
                               .FirstOrDefault();

            if (string.IsNullOrWhiteSpace(lastNumber)) return "GR0001";

            var newSequence = Convert.ToInt32(lastNumber.Substring(2, 4)) + 1;

            var newNumber = $"GR{newSequence.ToString("0000")}";

            return newNumber;
        }

        private void Validate(GoodsReceiptResource grRes)
        {
            if (grRes.DocumentDate == default)
                throw new Exception("Document Date cannot be empty.");

            if (grRes.WarehouseId == null)
                throw new Exception("Warehouse cannot be empty");

            foreach (var detail in grRes.ListDetail)
            {
                if (detail.ItemId == null)
                    throw new Exception("Item cannot be empty.");

                if (detail.Quantity == 0)
                    throw new Exception("Quantity cannot be empty.");
            }
        }

        public Guid Create(GoodsReceiptResource grRes)
        {
            Validate(grRes);

            var gr = new GoodsReceipt();
            gr.Id = Guid.NewGuid();
            gr.DocumentNumber = GetNewDocumentNumber();
            gr.DocumentDate = grRes.DocumentDate;
            gr.WarehouseId = grRes.WarehouseId;
            gr.Remarks = grRes.Remarks;
            db.GoodsReceipt.Add(gr);

            foreach (var detailRes in grRes.ListDetail)
            {
                var detail = new GoodsReceiptDetail();
                detail.Id = Guid.NewGuid();
                detail.GoodsReceiptId = gr.Id;
                detail.ItemId = detailRes.ItemId;
                detail.Quantity = detailRes.Quantity;
                db.GoodsReceiptDetail.Add(detail);
            }

            db.SaveChanges();

            return gr.Id;
        }

        public Guid Update(GoodsReceiptResource grRes)
        {
            Validate(grRes);

            // delete old detail
            var listOldDetailId = db.GoodsReceiptDetail
                                    .Where(x => x.GoodsReceiptId == grRes.Id)
                                    .Select(x => x.Id)
                                    .ToList();

            foreach (var detailId in listOldDetailId)
            {
                var detail = db.GoodsReceiptDetail.Find(detailId);
                db.GoodsReceiptDetail.Remove(detail);
            }
            // end delete old detail

            // update
            var gr = db.GoodsReceipt.Find(grRes.Id);
            gr.DocumentDate = grRes.DocumentDate;
            gr.WarehouseId = grRes.WarehouseId;
            gr.Remarks = grRes.Remarks;

            foreach (var detailRes in grRes.ListDetail)
            {
                var detail = new GoodsReceiptDetail();
                detail.Id = Guid.NewGuid();
                detail.GoodsReceiptId = gr.Id;
                detail.ItemId = detailRes.ItemId;
                detail.Quantity = detailRes.Quantity;
                db.GoodsReceiptDetail.Add(detail);
            }

            db.SaveChanges();

            return gr.Id;
        }

        public void Delete(Guid? id)
        {
            var listOldDetailId = db.GoodsReceiptDetail
                                    .Where(x => x.GoodsReceiptId == id)
                                    .Select(x => x.Id)
                                    .ToList();

            foreach (var detailId in listOldDetailId)
            {
                var detail = db.GoodsReceiptDetail.Find(detailId);
                db.GoodsReceiptDetail.Remove(detail);
            }

            var gr = db.GoodsReceipt.Find(id);
            db.GoodsReceipt.Remove(gr);

            db.SaveChanges();
        }
    }
}
