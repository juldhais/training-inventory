﻿using Inventory.Models;
using Inventory.Repositories;
using Inventory.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inventory.Services
{
    public class UserService
    {
        private readonly DataContext db;

        public UserService(DataContext db)
        {
            this.db = db;
        }

        public User Get(Guid? id)
        {
            return db.User.Find(id);
        }

        public List<User> GetList()
        {
            return db.User.ToList();
        }

        public List<User> GetList(string keyword)
        {
            var query = db.User.AsQueryable();

            if (!string.IsNullOrWhiteSpace(keyword))
                query = query.Where(x => x.UserName.Contains(keyword) || x.Role.Contains(keyword));

            return query.ToList();
        }

        private void Validate(User userResource)
        {
            if (string.IsNullOrWhiteSpace(userResource.UserName))
                throw new Exception("User name cannot be empty.");

            if (string.IsNullOrWhiteSpace(userResource.Password))
                throw new Exception("Password cannot be empty.");

            if (db.User.Any(x => x.UserName == userResource.UserName && x.Id != userResource.Id))
                throw new Exception("User name is already taken.");
        }

        public Guid Create(User userResource)
        {
            Validate(userResource);

            var userModel = new User();
            userModel.Id = Guid.NewGuid();
            userModel.UserName = userResource.UserName;
            userModel.Password = userResource.Password;
            userModel.Role = userResource.Role;
            db.User.Add(userModel);
            db.SaveChanges();

            return userModel.Id;
        }

        public Guid Update(User userResource)
        {
            Validate(userResource);

            var userModel = db.User.Find(userResource.Id);
            userModel.Id = Guid.NewGuid();
            userModel.UserName = userResource.UserName;
            userModel.Password = userResource.Password;
            userModel.Role = userResource.Role;
            db.SaveChanges();

            return userModel.Id;
        }

        public void Delete(Guid id)
        {
            var userModel = db.User.Find(id);
            db.Remove(userModel);
            db.SaveChanges();
        }

        public User Login(LoginResource loginResource)
        {
            if (string.IsNullOrWhiteSpace(loginResource.UserName))
                throw new Exception("User name cannot be empty.");

            if (string.IsNullOrWhiteSpace(loginResource.Password))
                throw new Exception("Password cannot be empty.");

            var user = db.User.FirstOrDefault(x => x.UserName == loginResource.UserName && x.Password == loginResource.Password);

            if (user == null)
                throw new Exception("User name or password did not match.");

            return user;
        }
    }
}
