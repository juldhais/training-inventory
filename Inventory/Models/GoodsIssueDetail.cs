﻿using System;

namespace Inventory.Models
{
    public class GoodsIssueDetail
    {
        public Guid Id { get; set; }
        public GoodsIssue GoodsIssue { get; set; }
        public Guid? GoodsIssueId { get; set; }
        public Item Item { get; set; }
        public Guid? ItemId { get; set; }
        public decimal Quantity { get; set; }
    }
}
