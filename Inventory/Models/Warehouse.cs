﻿using System;

namespace Inventory.Models
{
    public class Warehouse
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
