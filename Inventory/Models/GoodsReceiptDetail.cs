﻿using System;

namespace Inventory.Models
{
    public class GoodsReceiptDetail
    {
        public Guid Id { get; set; }
        public GoodsReceipt GoodsReceipt { get; set; }
        public Guid? GoodsReceiptId { get; set; }
        public Item Item { get; set; }
        public Guid? ItemId { get; set; }
        public decimal Quantity { get; set; }
    }
}
