﻿using System;

namespace Inventory.Models
{
    public class GoodsReceipt
    {
        public Guid Id { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public Warehouse Warehouse { get; set; }
        public Guid? WarehouseId { get; set; }
        public string Remarks { get; set; }

    }
}
