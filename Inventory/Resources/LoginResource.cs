﻿using System;

namespace Inventory.Resources
{
    public class LoginResource
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
    }
}
