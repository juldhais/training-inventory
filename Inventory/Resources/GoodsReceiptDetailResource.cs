﻿using System;

namespace Inventory.Resources
{
    public class GoodsReceiptDetailResource
    {
        public Guid Id { get; set; }
        public Guid? GoodsReceiptId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Unit { get; set; }
        public Guid? ItemId { get; set; }
        public decimal Quantity { get; set; }
    }
}
