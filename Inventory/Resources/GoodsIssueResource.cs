﻿using System;
using System.Collections.Generic;

namespace Inventory.Resources
{
    public class GoodsIssueResource
    {
        public Guid Id { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public Guid? WarehouseId { get; set; }
        public string Remarks { get; set; }

        public List<GoodsIssueDetailResource> ListDetail { get; set; }
    }
}
