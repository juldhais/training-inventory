﻿using System;

namespace Inventory.Resources
{
    public class GoodsIssueDetailResource
    {
        public Guid Id { get; set; }
        public Guid? GoodsIssueId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Unit { get; set; }
        public Guid? ItemId { get; set; }
        public decimal Quantity { get; set; }
    }
}
