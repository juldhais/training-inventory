﻿using System;

namespace Inventory.Resources
{
    public class StockReportResource
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public decimal Beginning { get; set; }
        public decimal In { get; set; }
        public decimal Out { get; set; }
        public decimal Ending { get; set; }
    }
}