﻿using Inventory.Models;
using Microsoft.EntityFrameworkCore;

namespace Inventory.Repositories
{
    public class DataContext : DbContext
    {
        public DataContext() { }
        public DataContext(DbContextOptions options) : base(options) { }

        public DbSet<Item> Item { get; set; }
        public DbSet<Warehouse> Warehouse { get; set; }

        public DbSet<User> User { get; set; }

        public DbSet<GoodsReceipt> GoodsReceipt { get; set; }
        public DbSet<GoodsReceiptDetail> GoodsReceiptDetail { get; set; }

        public DbSet<GoodsIssue> GoodsIssue { get; set; }
        public DbSet<GoodsIssueDetail> GoodsIssueDetail { get; set; }
    }
}
