﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Repositories
{
    public class DataInitializer
    {
        private readonly DataContext db;

        public DataInitializer(DataContext db)
        {
            this.db = db;
        }

        public void Run()
        {
            if (db.User.Any()) return;

            db.User.Add(new User
            {
                UserName = "admin",
                Password = "admin",
                Role = "Administrator"
            });

            db.SaveChanges();
        }
    }
}
