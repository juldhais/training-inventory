﻿using Inventory.Models;
using Inventory.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Inventory.Controllers
{
    public class ItemController : Controller
    {
        private readonly ItemService itemService;

        public ItemController(ItemService itemService)
        {
            this.itemService = itemService;
        }

        [Authorize]
        public IActionResult Index(string keyword)
        {
            ViewBag.Keyword = keyword;

            var itemList = itemService.GetList(keyword);
            
            return View(itemList);
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Create()
        {
            ViewBag.Action = "Create";
            return View("CreateUpdate");
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IActionResult Create(Item item)
        {
            try
            {
                itemService.Create(item);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Action = "Create";
                ViewBag.ErrorMessage = ex.Message;
                return View("CreateUpdate", item);
            }
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Update(Guid id)
        {
            ViewBag.Action = "Update";
            var item = itemService.Get(id);
            return View("CreateUpdate", item);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IActionResult Update(Item item)
        {
            try
            {
                itemService.Update(item);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Action = "Update";
                ViewBag.ErrorMessage = ex.Message;
                return View("CreateUpdate", item);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IActionResult Delete(Guid id)
        {
            itemService.Delete(id);
            return Ok();
        }

        [Authorize]
        public IActionResult Get(Guid id)
        {
            var item = itemService.Get(id);
            return Ok(item);
        }
    }
}