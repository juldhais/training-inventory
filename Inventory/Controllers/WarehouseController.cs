﻿using Inventory.Models;
using Inventory.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Inventory.Controllers
{
    public class WarehouseController : Controller
    {
        private readonly WarehouseService warehouseService;

        public WarehouseController(WarehouseService warehouseService)
        {
            this.warehouseService = warehouseService;
        }

        [Authorize]
        public IActionResult Index(string keyword)
        {
            ViewBag.Keyword = keyword;

            var warehouseList = warehouseService.GetList(keyword);
            return View(warehouseList);
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Create()
        {
            ViewBag.Action = "Create";
            return View("CreateUpdate");
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IActionResult Create(Warehouse warehouse)
        {
            try
            {
                warehouseService.Create(warehouse);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Action = "Create";
                ViewBag.ErrorMessage = ex.Message;
                return View("CreateUpdate", warehouse);
            }
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Update(Guid id)
        {
            ViewBag.Action = "Update";
            var warehouse = warehouseService.Get(id);
            return View("CreateUpdate", warehouse);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IActionResult Update(Warehouse warehouse)
        {
            try
            {
                warehouseService.Update(warehouse);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Action = "Update";
                ViewBag.ErrorMessage = ex.Message;
                return View("CreateUpdate", warehouse);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IActionResult Delete(Guid id)
        {
            warehouseService.Delete(id);
            return Ok();
        }
    }
}