﻿using System;
using Inventory.Resources;
using Inventory.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventory.Controllers
{
    public class GoodsIssueController : Controller
    {
        private readonly GoodsIssueService giService;
        private readonly WarehouseService warehouseService;
        private readonly ItemService itemService;

        public GoodsIssueController(GoodsIssueService giService, 
                                      WarehouseService warehouseService,
                                      ItemService itemService)
        {
            this.giService = giService;
            this.warehouseService = warehouseService;
            this.itemService = itemService;
        }

        [Authorize]
        public IActionResult Index(Guid? warehouseId, DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null) startDate = DateTime.Today;
            if (endDate == null) endDate = DateTime.Today;

            ViewBag.WarehouseId = warehouseId;
            ViewBag.StartDate = startDate.Value.ToString("yyyy-MM-dd");
            ViewBag.EndDate = endDate.Value.ToString("yyyy-MM-dd");
            ViewBag.WarehouseList = warehouseService.GetList();

            var list = giService.GetList(warehouseId, startDate, endDate);

            return View(list);
        }

        [Authorize]
        public IActionResult Create()
        {
            ViewBag.WarehouseList = warehouseService.GetList();
            ViewBag.ItemList = itemService.GetList();

            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult Create(GoodsIssueResource grRes)
        {
            try
            {
                giService.Create(grRes);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.GetBaseException().Message);
            }
        }

        [Authorize]
        public IActionResult Get(Guid? id)
        {
            var grRes = giService.Get(id);
            return Ok(grRes);
        }

        [Authorize]
        public IActionResult Update(Guid id)
        {
            ViewBag.WarehouseList = warehouseService.GetList();
            ViewBag.ItemList = itemService.GetList();
            ViewBag.Id = id;

            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult Update(GoodsIssueResource grRes)
        {
            try
            {
                giService.Update(grRes);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.GetBaseException().Message);
            }
        }

        [HttpPost]
        [Authorize]
        public IActionResult Delete(Guid id)
        {
            giService.Delete(id);
            return Ok();
        }

        [Authorize]
        public IActionResult Print(Guid id)
        {
            var res = giService.Get(id);
            return View(res);
        }
    }
}