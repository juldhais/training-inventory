﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Inventory.Resources;
using Inventory.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventory.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserService userService;

        public HomeController(UserService userService)
        {
            this.userService = userService;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(string returnUrl)
        {
            var model = new LoginResource();
            model.ReturnUrl = returnUrl;

            return View(model);
        }

        [HttpPost]
        public IActionResult Login(LoginResource loginResource)
        {
            try
            {
                var user = userService.Login(loginResource);

                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, user.UserName));
                claims.Add(new Claim(ClaimTypes.Role, user.Role));
                claims.Add(new Claim("UserId", user.Id.ToString()));

                var identity = new ClaimsIdentity(claims, "Cookies");
                var principal = new ClaimsPrincipal(identity);

                HttpContext.SignInAsync(principal);

                if (!string.IsNullOrWhiteSpace(loginResource.ReturnUrl))
                    return Redirect(loginResource.ReturnUrl);
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.GetBaseException().Message;
                return View(loginResource);
            }
        }

        [Authorize]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();

            return RedirectToAction("Login");
        }

        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}