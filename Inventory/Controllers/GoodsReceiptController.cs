﻿using System;
using Inventory.Resources;
using Inventory.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventory.Controllers
{
    public class GoodsReceiptController : Controller
    {
        private readonly GoodsReceiptService grService;
        private readonly WarehouseService warehouseService;
        private readonly ItemService itemService;

        public GoodsReceiptController(GoodsReceiptService grService, 
                                      WarehouseService warehouseService,
                                      ItemService itemService)
        {
            this.grService = grService;
            this.warehouseService = warehouseService;
            this.itemService = itemService;
        }

        [Authorize]
        public IActionResult Index(Guid? warehouseId, DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null) startDate = DateTime.Today;
            if (endDate == null) endDate = DateTime.Today;

            ViewBag.WarehouseId = warehouseId;
            ViewBag.StartDate = startDate.Value.ToString("yyyy-MM-dd");
            ViewBag.EndDate = endDate.Value.ToString("yyyy-MM-dd");
            ViewBag.WarehouseList = warehouseService.GetList();

            var list = grService.GetList(warehouseId, startDate, endDate);

            return View(list);
        }

        [Authorize]
        public IActionResult Create()
        {
            ViewBag.WarehouseList = warehouseService.GetList();
            ViewBag.ItemList = itemService.GetList();

            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult Create(GoodsReceiptResource grRes)
        {
            try
            {
                grService.Create(grRes);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.GetBaseException().Message);
            }
        }

        [Authorize]
        public IActionResult Get(Guid? id)
        {
            var grRes = grService.Get(id);
            return Ok(grRes);
        }

        [Authorize]
        public IActionResult Update(Guid id)
        {
            ViewBag.WarehouseList = warehouseService.GetList();
            ViewBag.ItemList = itemService.GetList();
            ViewBag.Id = id;

            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult Update(GoodsReceiptResource grRes)
        {
            try
            {
                grService.Update(grRes);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.GetBaseException().Message);
            }
        }

        [HttpPost]
        [Authorize]
        public IActionResult Delete(Guid id)
        {
            grService.Delete(id);
            return Ok();
        }

        [Authorize]
        public IActionResult Print(Guid id)
        {
            var res = grService.Get(id);
            return View(res);
        }
    }
}