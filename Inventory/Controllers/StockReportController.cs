﻿using Inventory.Resources;
using Inventory.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Inventory.Controllers
{
    public class StockReportController : Controller
    {
        private readonly StockReportService stockReportService;
        private readonly ItemService itemService;
        private readonly WarehouseService warehouseService;

        public StockReportController(StockReportService stockReportService, 
                                     ItemService itemService, 
                                     WarehouseService warehouseService)
        {
            this.stockReportService = stockReportService;
            this.itemService = itemService;
            this.warehouseService = warehouseService;
        }

        [Authorize]
        public IActionResult Index(Guid? itemId, Guid? warehouseId, DateTime? startDate, DateTime? endDate)
        {
            try
            {
                ViewBag.WarehouseId = warehouseId;
                ViewBag.ItemId = itemId;
                ViewBag.StartDate = startDate == null ? "" : startDate.Value.ToString("yyyy-MM-dd");
                ViewBag.EndDate = endDate == null ? "" : endDate.Value.ToString("yyyy-MM-dd");
                ViewBag.ItemList = itemService.GetList();
                ViewBag.WarehouseList = warehouseService.GetList();

                var result = stockReportService.GetReport(itemId, warehouseId, startDate, endDate);
                return View(result);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.GetBaseException().Message;
                return View(new List<StockReportResource>());
            }
        }
    }
}
