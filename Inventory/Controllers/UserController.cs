﻿using Inventory.Models;
using Inventory.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Inventory.Controllers
{
    public class UserController : Controller
    {
        private readonly UserService userService;

        public UserController(UserService userService)
        {
            this.userService = userService;
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Index(string keyword)
        {
            ViewBag.Keyword = keyword;

            var userList = userService.GetList(keyword);
            
            return View(userList);
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Create()
        {
            ViewBag.Action = "Create";
            return View("CreateUpdate");
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IActionResult Create(User user)
        {
            try
            {
                userService.Create(user);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Action = "Create";
                ViewBag.ErrorMessage = ex.Message;
                return View("CreateUpdate", user);
            }
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Update(Guid id)
        {
            ViewBag.Action = "Update";
            var user = userService.Get(id);
            return View("CreateUpdate", user);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IActionResult Update(User user)
        {
            try
            {
                userService.Update(user);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Action = "Update";
                ViewBag.ErrorMessage = ex.Message;
                return View("CreateUpdate", user);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IActionResult Delete(Guid id)
        {
            userService.Delete(id);
            return Ok();
        }
    }
}